import styled from "@emotion/styled";
import React from "react";
import {Route, Routes} from "react-router-dom";
import BeerLayout from "./components/BeerLayout";
import AllBeers from "./pages/AllBeers";
import BeerDetails from "./pages/BeerDetails";
import Home from "./pages/Home";

type AppProps = {}

const StyledApp = styled.div`

`;

const App: React.FC<AppProps> = () => {
	return (
		<StyledApp>
			<Routes>
				<Route path={"/"} element={<Home/>}/>
				<Route path={"/beer/*"} element={<BeerLayout/>}>
						<Route path={""} element={<AllBeers/>}/>
						<Route path={":id"} element={<BeerDetails/>}/>
				</Route>
			</Routes>
		</StyledApp>
	);
};

export default App;
