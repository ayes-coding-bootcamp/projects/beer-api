import { Global, css } from "@emotion/react";

const GlobalStyle = () => (
	<Global
		styles={css`
		  @font-face {
			font-family: "Roboto-Slab";
			src: url("src/assets/fonts/RobotoSlab-VariableFont_wght.ttf");
			font-weight: 100 900;
		  }

		  * {
			margin: 0;
			padding: 0;
			box-sizing: border-box;
		  }

		  html,
		  body {
			font-family: "Roboto-Slab", sans-serif;
		  }

		  body {
			padding: 1rem;
		  }
		`}
	/>
);

export default GlobalStyle;