import styled from "@emotion/styled";
import React from "react";
import {Link} from "react-router-dom";
import {Beer} from "../data/typedefs";

type BeerItemProps = {
	beer: Beer;
}

const StyledBeerItem = styled.article`
  display: flex;
  padding: 2rem 0;
  border-bottom: 2px solid grey;
  justify-content: space-around;

  img {
	
	height: 30vh;
	width: 20%;
	object-fit: contain;
  }

  article {
	width: 60%;
	
	display: flex;
	flex-direction: column;
	justify-content: space-around;
	
	gap: 1rem;
	
	hgroup {
	  h2 {
		
	  }
	  h3 {
		color: #FFCA41;
	  }
	}
	p {
	  font-weight: 600;
	}
	a {
	  color: white;
	  background: #FFCA41;
	  border-radius: 99999px;
	  padding: 8px;
	  text-align: center;
	  width: 120px;
	  text-decoration: inherit;
	 
	}
  }
`;

const BeerItem: React.FC<BeerItemProps> = ({beer}) => {
	return (
		<StyledBeerItem>
			<img src={beer.image_url} alt={`${beer.name}-image`}/>
			<article>
				<hgroup>
					<h2>
						{beer.name}
					</h2>
					<h3>
						{beer.tagline}
					</h3>
				</hgroup>
				<p>
					Created by: {beer.contributed_by}
				</p>
				<Link to={`/beer/${beer._id}`} state={beer}>
					Details
				</Link>
			</article>
		</StyledBeerItem>
	);
};

export default BeerItem;
