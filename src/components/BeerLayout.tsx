import React, {ReactNode} from "react";
import styled from "@emotion/styled";
import {Link, Outlet} from "react-router-dom";
import HomeImage from "../../public/assets/img/beer-icon.svg";

type BeerLayoutProps = {

}

const StyledBeerLayout = styled.div`
  footer {
	display: block;
	background: #FFCA41;
	position: fixed;
	bottom: 0;
	left: 0;
	right: 0;

	img {

	  width: 3rem;
	  aspect-ratio: 1 / 1;
	  display: block;
	  background: white;
	  padding: 6px;
	  border-radius: 50%;
	  margin: 0.5rem auto;
	}
  }
`;

const BeerLayout: React.FC<BeerLayoutProps> = () => {
	return (
		<StyledBeerLayout>
			<Outlet/>
			<footer>
				<Link to={"/"}>
					<img src={HomeImage} alt="beer-icon" />
				</Link>
			</footer>
		</StyledBeerLayout>
	);
};

export default BeerLayout;
