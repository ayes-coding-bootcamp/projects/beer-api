import styled from "@emotion/styled";
import React, {useEffect, useState} from "react";
import {Beer} from "../data/typedefs";
import BeerItem from "./BeerItem";

type BeerListProps = {
	beers: Beer[];
}

const StyledBeerList = styled.section`
  display: flex;
  flex-direction: column;
  gap: 1rem;
  max-width: 550px;
  margin: 0 auto;
`;

const BeerList: React.FC<BeerListProps> = ({beers}) => {
	return (
		<StyledBeerList>
			{beers.map((item: Beer) => {
				return (
					<BeerItem
						key={item._id}
						beer={item}
					/>
				)
			})}
		</StyledBeerList>
	);
};

export default BeerList;
