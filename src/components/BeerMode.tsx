import React from 'react';
import styled from "@emotion/styled";
import {Link} from "react-router-dom";

type BeerModeProps = {
	image: string;
	imageAlt: string;
	title: string;
	text: string;
	link: string;
}

const StyledBeerMode = styled.article`
  display: flex;
  flex-direction: column;
  text-align: center;
  margin: 0 auto;
  max-width: 250px;
  width: 100%;

  a {
	color: inherit;
	text-decoration: inherit;
	figure {
	  padding: 0;
	  background: #FFCA41;

	  img {
		display: block;
		margin: 0 auto;
		width: 100%;
		object-fit: cover;
	  }

	  figcaption {
		font-size: 1.4rem;
		font-weight: 600;
		padding: 0.6rem;
		color: inherit;
	  }
	}
  }
  
  p {
	padding: 1rem;
  }
`;

const BeerMode: React.FC<BeerModeProps> = ({image, imageAlt, title, text, link}) => {
	return (
		<StyledBeerMode className={"beerMode"}>
			<Link to={link}>
				<figure>
					<img src={image} alt={imageAlt}/>
					<figcaption>
						{title}
					</figcaption>
				</figure>
			</Link>
			<p>
				{text}
			</p>
		</StyledBeerMode>
	);
};

export default BeerMode;
