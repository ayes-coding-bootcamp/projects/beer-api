import styled from "@emotion/styled";
import React, {useEffect, useState} from "react";
import {Route, Routes} from "react-router-dom";
import BeerLayout from "../components/BeerLayout";
import BeerList from "../components/BeerList";
import {Beer} from "../data/typedefs";
import BeerDetails from "./BeerDetails";

type AllBeersProps = {}

const StyledAllBeers = styled.div`

`;

const AllBeers: React.FC<AllBeersProps> = () => {
	const [beers, setBeers] = useState([] as Beer[]);

	useEffect(() => {
		fetch("https://ih-beers-api2.herokuapp.com/beers")
			.then(response => response.json())
			.then(data => {
				setBeers(data);
			});
	}, []);

	return (
		<StyledAllBeers>
			<BeerList beers={beers}/>
		</StyledAllBeers>
	);
};

export default AllBeers;
