import styled from "@emotion/styled";
import React, {useEffect, useState} from "react";
import {Link, useLocation, useNavigate} from "react-router-dom";
import {Beer} from "../data/typedefs";
import returnImage from "../../public/assets/img/back-icon.svg";

type BeerDetailsProps = {}

const StyledBeerDetails = styled.div`
  img {
	display: block;
	margin: 0 auto;
	height: 40vh
  }

  hgroup {
	h1 {
	  font-size: 2.5rem;
	}

	h2 {
	  font-size: 1.5rem;
	  color: #FFCA41;
	}
  }

  article {
	padding: 12px 0;

	& > div {
	  display: flex;
	  justify-content: space-between;

	}
  }

  a {
	display: block;
	aspect-ratio: 1 / 1;
	width: 60px;
	margin: 2rem 1rem;
	img{
	  padding: 12px;
	  height: inherit;
	  width: inherit;
	  border-radius: 50%;
	  background: #FFCA41;
	}
  }
`;

const BeerDetails: React.FC<BeerDetailsProps> = () => {
	const [beer, setBeer] = useState({} as Beer);
	const location = useLocation();
	const navigate = useNavigate();

	useEffect(() => {
		if (location.pathname === "/beer/random") {
			fetch("https://ih-beers-api2.herokuapp.com/beers/random")
				.then(response => response.json())
				.then(json => {
					console.log(json);
					setBeer(json);
				});
		} else {
			setBeer(location.state);
		}
	}, []);

	const goBack = (event: React.MouseEvent) => {
		event.preventDefault();
		navigate(-1);
	}

	return (
		<StyledBeerDetails>
			<img src={beer.image_url} alt={`${beer.name}-image`}/>
			<hgroup>
				<h1>
					{beer.name}
				</h1>
				<h2>
					{beer.tagline}
				</h2>
			</hgroup>
			<article>
				<div>
					<p>
						First brewed:
					</p>
					<p>
						{beer.first_brewed}
					</p>
				</div>
				<div>
					<p>
						Attenuation level:
					</p>
					<p>
						{beer.attenuation_level}
					</p>
				</div>
			</article>
			<p>
				{beer.description}
			</p>
			<q style={{display: beer.brewers_tips ? "visible" : "hidden"}}>
				{beer.brewers_tips}
			</q>
			<Link to="#" onClick={goBack}>
				<img src={returnImage} alt="return"/>
			</Link>
		</StyledBeerDetails>
	);
};

export default BeerDetails;
