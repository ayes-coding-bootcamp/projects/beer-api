import styled from "@emotion/styled";
import React from "react";
import BeerMode from "../components/BeerMode";
import allBeersImage from "../../public/assets/img/all-beers.png";
import randomBeerImage from "../../public/assets/img/random-beer.png";

type HomeProps = {}

const StyledHome = styled.main`
  padding: 2rem;
  display: flex;
  flex-direction: column;

  justify-content: space-around;
`;

const Home: React.FC<HomeProps> = () => {


	return (
		<StyledHome>
			<BeerMode image={allBeersImage} imageAlt={"All Beers Image"}
					  text={"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores autem consectetur consequatur eos."}
					  title={"All Beers"} link={"/beer"}/>
			<BeerMode image={randomBeerImage} imageAlt={"Random Beer Image"}
					  text={"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores autem consectetur consequatur eos."}
					  title={"Random Beer"} link={`/beer/random`}/>
		</StyledHome>
	);
};

export default Home;
